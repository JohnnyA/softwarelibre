# Laravel From The Scratch  Controllers Techniques
## En esta parte se utiliza correctamente el modelo de ruta el cual se creran funciones para poner en practica
### se crea un archivo 'ArticlesController.php' el cual contien lo siguiente.
![VirtualBox](imagenes/ima1.jpg)
![VirtualBox](imagenes/ima2.jpg)
![VirtualBox](imagenes/ima3.jpg)
### La siguiente técnica es reducir la duplicación. Si revisa nuestro CurrentArticlesController, hacemos referencia a las claves de solicitud en varios lugares. Ahora bien, resulta que hay una forma útil de reducir considerablemente esta repetición.
### Para poder resumir el codigo se cambiaran algunas varibles, el cual la funcion es dejar de repetir atributos.
![VirtualBox](imagenes/ima4.jpg)
### para validar cada uno de los parametros se utilizo la siguiente funcion
![VirtualBox](imagenes/ima5.jpg)

# Laravel From The Scratch Eloquent
## Volvamos ahora a Eloquent y comencemos a discutir las relaciones. Por ejemplo, si tengo una instancia de $ user, ¿cómo puedo recuperar todos los proyectos creados por ese usuario? O si, en cambio, tengo una instancia de $ project, ¿cómo buscaría al usuario que administra ese proyecto?
### para empezar con este ejemplo primero se crearan dos archivos, 'user.php' y 'project.php' el cual contendran los siguientes datos.

### user
![VirtualBox](imagenes/ima6.jpg)
### project
![VirtualBox](imagenes/ima7.jpg)
### Relaciones de muchos a muchos con tablas de enlace
### para realizar estos ejemplos se tendra realizar los siguientes pasos.
### dentro del archivo 'Article.php'
![VirtualBox](imagenes/ima8.jpg)
### como se puede observar en la imagen se crea una tabla tags, en su base de datos se debe de configurar como usted lo tiene estructurado. 
### tambien debera de crear el archivo tah.php
![VirtualBox](imagenes/ima9.jpg)

# Laravel From The Scratch  Authentication
## Gracias al paquete de primera persona, Laravel UI, puede crear fácilmente un sistema de registro completo que incluye registros, manejo de sesiones, restablecimiento de contraseñas, confirmaciones de correo electrónico y más. Y la mejor parte es que puede eliminar este tedioso y común requisito ... en minutos.

### para empezar con este ejemplo lo primero que hay que hacer es ejecutar este comando 'laravel new laravel6'
### acontinuacion digitamos el siguiente 'composer require laravel/ui --dev'
### php artisan ui vue --auth
### npm install && npm run dev
### seguidamente en el archivo package.json podran ver todas las dependencias que se instalaron















