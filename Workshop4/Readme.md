# Laravel From The Scratch  Mail
## La forma más fácil de enviar un correo electrónico en Laravel es con el método Mail :: raw (). En esta lección, aprenderemos cómo enviar un formulario, leer una dirección de correo electrónico proporcionada en la solicitud y luego enviar un correo electrónico a la persona.
### Para empezar lo primero que tienes que hacer es crear los archivos que contendran cada una de las funciones.
### dentro de Controller se crea un archivo 'ContacController'
![archivo conctac](imagenes/contac.jpg)
### dentro de view se crea el archivo 'contact.blade.php'
#### Este contiene la inteferza del formulario de email.

## Simulate an Inbox using Mailtrap
### Es útil ver un registro de cualquier correo que se envía mientras está en modo de desarrollo, pero cambiemos al uso de Mailtrap. Esto nos permitirá simular una bandeja de entrada de correo electrónico de la vida real, que será especialmente útil una vez que comencemos a enviar correo electrónico HTML.
### puedes probar esta herramienta en el siguinete link.
[mailtrap](https://mailtrap.io/)

## Send HTML Emails Using Mailable Classes
### Hasta ahora, solo hemos logrado enviar un correo electrónico básico en texto plano. Actualicemos a un correo electrónico completo basado en HTML aprovechando las clases de correo electrónico de Laravel.
### para empzar primero se ejecuta el siguiente comando 'php artisan make:mail ContactMe' el cual creara el siguiente archivo.
![Archivos de Mail](imagenes/mail1.jpg)
### seguidamente en 'ContactController' se agreaga lo siguiente.
![contact controller Mail](imagenes/email2.jpg)
### el archivo 'contacMe' tendra lo siguiente.
![contact controller Mail](imagenes/mail3.jpg)
### Al realizar todos esos cambios podras probar nuevamente en el [mailtrap](https://mailtrap.io/)

## Send Email Using Markdown Templates
### Alternativamente, podemos escribir correos electrónicos usando Markdown. En esta lección, aprenderá a enviar correos electrónicos con un formato agradable construido por el marco. Para los casos en los que necesite modificar la estructura HTML subyacente, también publicaremos los activos que se pueden enviar por correo y revisaremos cómo crear temas personalizados.

# Laravel From The Scratch  Notifications
## Una notificación puede enviarse a través de cualquier número de "canales". Quizás una notificación en particular debería alertar al usuario por correo electrónico y a través del sitio web. ¡Claro, no hay problema! Aprendamos cómo en este episodio.
### Para empezar los que se hara es seguir las instrucciones que se encuentra en la documentacion de laravel [Notificacion](https://laravel.com/docs/8.x/notifications#introduction).

# Laravel From The Scratch  Events
## Eventing Pros and Cons
### Los eventos ofrecen una forma para que una parte de su aplicación haga un anuncio que se propague por todo el sistema. En este episodio, no solo revisaremos lo esencial, sino que también discutiremos los pros y los contras de este enfoque particular para estructurar una aplicación.
### Para empezar lo primero que tiene que hacer es ejecutar el siguiente comando php artisan event:list
### lo que hace es presentar una lista en consola que muestra los eventos de Laravel proporcionan una implementación de observador simple, lo que le permite suscribirse y escuchar varios eventos que ocurren en su aplicación. Las clases de eventos generalmente se almacenan en el directorio app\events, mientras que sus oyentes se almacenan en app\listeners





