
# Descargar virtualbox

https://www.virtualbox.org/wiki/Downloads

## se procede a instalar el programa

# Descargar vagrant

https://www.vagrantup.com/downloads.html

## se procede a instalar el programa

# a continuacion procedemos a crear la carpeta buster con el siguiente comando

### mkdir buster

### dentro de la carpeta buster se inicializa vagrant con el siguiente comando.
### #vagrant init debian/buster64

### seguidamente se edita el archivo vagrant file, se digita los seguientes comandos

### code vangranfile, con este comando abrimos el archivo el visual code.

### A continuacion descomentamos la linea 40, como se muestra en la siguiente imagen.
![VirtualBox](Imagenes/edit-vagrantfile.jpg)

### luego se digita el comando "vagrant up" para iniciarlo.

### seguidamente se carga el agente con el siguiente comando

### eval $(ssh-agent -s)

### luego se agrega la llave privada con el siguiente comando

### ssh-add .vagrant/machines/default/virtualbox/private_key
### a continuacion se digite el siguiente comando para entrar a vagrantbuster
### ssh -p 2222 vagrant@localhost

### seguidamente digiatmos el comamdo "nano .bashrc" para editar un alias
![VirtualBox](Imagenes/edit-alias.jpg)

### se digita el siguiente comando para actualizar la lista de paquetes disponibles "sudo apt-get update"

### seguidamente se instala las dependencias de php con las siguientes comandos 
### sudo apt-get install php7.3 php7.3-bcmath php7.3-curl php7.3-json php7.3-mbstring php7.3-mysql php7.3-pgsql php7.3-xml php7.3-xmlrpc php7.3-zip git vim vim-nox curl zsh mariadb-server mariadb-client

### se crea una nueva carperta con el siguiente comando "sudo mkdir /opt/composer"

### luego se instala composer "$ curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/opt/composer"

### se digita este comando para hacer un enlace simbolico "sudo ln -s /opt/composer/composer.phar /usr/bin/composer"

# Instalacion de laravel

## a continuacion se ejecuta el siguiente comando para instalar laravel.
### composer global require laravel/installer

### instalacion de la funcion tree  sudo apt-get install tree
### se digita este comando para entrar a la configuracion del composer
### vim .bashrc

### se agrega la seguiente linea para configurar el composer 
![VirtualBox](Imagenes/editComposer.jpg)

### se ejecuta este comando para cargar los datos
### source .bashrc

### seguidamente se crea una carpeta y un proyecto en laravel como se muestra en la imagen.
![VirtualBox](Imagenes/creacionProyecto.jpg)

### comando para sincronizar las maquinas
### vagrant rsync

### sguidamente se digita este comando para reiciar apache sudo a2ensite proyecto.isw811.xyz.conf


