import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Dashboard");
    }

    async getHtml() {
        return `
            <h1>¿QUÉ es una web SPA? </h1>
            <p>
            Una web SPA (Single Page Application) es una página web la cual está todo el contenido en una sola página, es decir, carga tan solo un archivo HTML y todo se produce dentro de este único archivo. De esta manera se puede ofrecer una experiencia más fluida, más rápida.
            </p>
            <p/>
            Es decir, todo el código HTML, JavaScript y CSS se carga una sola vez, puedes navegar entre los diferentes apartados de la página y el contenido ya estará precargado de antemano, es por esto que es tan rápido. En algunos casos también lo que se hace es cargar estos contenidos dinámicamente si se requieren, pero no tiene que cargar toda la página de nuevo, tan solo los nuevos contenidos.
            </p>
            <p>
                <a href="/posts" data-link>Ver algunas publicaciones.</a>.
            </p>
        `;
    }
}