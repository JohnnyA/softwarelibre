import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Publicaciones");
    }

    async getHtml() {
        return `
            <h1>Publicaciones</h1>
            <p>Estás viendo las publicaciones!</p>
        `;
    }
}