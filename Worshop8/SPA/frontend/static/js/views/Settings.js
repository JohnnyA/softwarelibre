import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Configuraciones");
    }

    async getHtml() {
        return `
            <h1>Configuraciones.</h1>
            <p>Administre su privacidad y configuración.</p>
        `;
    }
}