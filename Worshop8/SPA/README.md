# Single page application
### es una página web la cual está todo el contenido en una sola página, es decir, carga tan solo un archivo HTML y todo se produce dentro de este único archivo. De esta manera se puede ofrecer una experiencia más fluida, más rápida.

### seguidamente se elaborara una serie de pasos para relizar un pequeño ejemplo.
1. se creara una carpeta "frontend" 
2. Dentro de la carpeta frontend se creara una carpeta que se llamara "static" y un archivo "index.html"
3. Dentro de la carpeta static se agregara dos carpetas "css" y "js"
1. La carpeta js contendra una carpeta "views" y un archivo "index.js" que tendra la siguiente estructura.
![estructura](imagenes/estructura.jpg)
2. Dentro de la carpeta views es donde se crearan todas las vista que estaran en el formato json
3. Se creara un archivo dentro del proyecto que se llamara "server.js" esto ejecutara el proyecto
![server](imagenes/server.jpg)
4. se ejecuta el comando "npm i express" para instalar todas las dependecias que se necesitaran para trabajar con json.

### Estructura de todo el proyecto.

![Estructura](imagenes/pro.jpg)




