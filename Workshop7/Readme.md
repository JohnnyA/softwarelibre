# Hello Heroku
### Lo primero que hay que hacer es crear un proyecto en laravel, se crea con el siguinete comando.
### "composer create-project laravel/laravel laravelHeroku"
### Seguidamente se ingresa a la caperta del proyecto
### "cd laravelHeroku"
### Laravel por defecto  ignora al composer.lock, es por esto que debemos eliminar la referencia que se hace a él editando el archivo .gitignore que se encuentra en el directorio principal de nuestro proyecto (en nuestro caso laravelHeroku) y eliminando la línea que dice composer.lock.
### Agregamos el archivo Procfile 
### Crea el archivo Procfile en el directorio principal de tu proyecto (sin extensión y con la P mayúscula), y dentro de éste coloca lo siguiente.
### "web: vendor/bin/heroku-php-apache2 public/"
###  Inicializamos nuestro repositorio en Git
### "git init"
### "git add ."
### "git commit -m "Commit inicial de Laravel sobre Heroku""

### Ejecutamos este comando en el directorio principal del proyecto:
### "heroku create"

### "git push heroku master" en algunos casos puede ser git push origin master, o el nombre que uno le tenga.