 -- -----------------------------------------------------
-- reto 4
-- -----------------------------------------------------
 DESC invoices; DESC orders;  DESC order_details; DESC products; DESC employees;
SELECT count(1) as `Cantidad`,
	concat(e.first_name, ' ', e.last_name) as `Vendedor`,
    round(sum(od.unit_price * od.quantity),2) as `Monto Facturado`
FROM invoices i
JOIN orders o
ON o.id = i.order_id
JOIN order_details od
ON od.order_id = o.id
JOIN employees e
ON e.id = o.employee_id
WHERE o.status_id <> 0
AND od.status_id in (2,3)
GROUP BY o.employee_id
ORDER BY count(1) DESC, concat(e.first_name, ' ', e.last_name) ASC;

 -- -----------------------------------------------------
-- reto 5
-- -----------------------------------------------------
SELECT product_code as codigo, product_name as producto, quantity as cantidad 
FROM products p, inventory_transactions i 
where p.id = i.product_id and
 i.transaction_type = 1 group by i.product_id;

 -- -----------------------------------------------------
-- reto 6
-- -----------------------------------------------------

SELECT product_code as codigo, product_name as producto,quantity as cantidad 
FROM products p
join inventory_transactions i 
where p.id = i.product_id and
 i.transaction_type  in (2,3,4)
 group by p.id;


-- -----------------------------------------------------
-- reto 7
-- -----------------------------------------------------
SELECT product_code as codigo, product_name as producto, type_name as tipo,
 transaction_created_date as fecha, sum(quantity) as cantidad 
FROM products p
 JOIN inventory_transactions i
 ON p.id = i.product_id
 JOIN inventory_transaction_types it
 ON i.transaction_type = it.id 
WHERE transaction_created_date BETWEEN '2006/03/22' AND '2006/03/24'
GROUP BY p.product_name, i.transaction_type;



