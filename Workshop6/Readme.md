 # SQL con Northwind

 ## 1- Recupere el código (id) y la descripción (type_name) de los tipos de movimiento de inventario (inventory_transaction_types).
 ![sql](imagenes/reto1.jpg)

## 2- Recupere la cantidad de ordenes (orders)registradas por cada vendedor (employees).
 ![sql](imagenes/reto2.jpg)

## 3- Recupere la lista de los 10 productos más ordenados (order_details), y la cantidad total de unidades ordenadas para cada uno de los productos.
 ![sql](imagenes/reto3.jpg)
 ## Deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la cantidad de unidades.

 ## 4- Recupere el monto total (invoices, orders, order_details, products) y la cantidad de facturas (invoices) por vendedor (employee). Debe considerar solamente las ordenes con estado diferente de 0 y solamente los detalles en estado 2 y 3, debe utilizar el precio unitario de las lineas de detalle de orden, no considere el descuento, no considere los impuestos, porque la comisión a los vendedores se paga sobre el precio base.
 ![sql](imagenes/reto4.1.jpg)
 ![sql](imagenes/reto4.2.jpg)

 ## 5- Recupere los movimientos de inventario del tipo ingreso. Tomando como base todos los movimientos de inventario (inventory_transactions), considere unicamente el tipo de movimiento 1 (transaction_type) como ingreso.
![sql](imagenes/reto5.png)
  ## Debe agrupar por producto (inventory_transactions.product_id) y deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la cantidad de unidades ingresadas.

  ## 6- Recupere los movimientos de inventario del tipo salida. Tomando como base todos los movimientos de inventario (inventory_transactions), considere unicamente los tipos de movimiento (transaction_type) 2, 3 y 4 como salidas.
![sql](imagenes/reto6v.jpg)
![sql](imagenes/reto6f.jpg)
## Debe agrupar por producto (products) y deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la cantidad de unidades que salieron.

## 7- Genere un reporte de movimientos de inventario (inventory_transactions) por producto (products), tipo de transacción y fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas fechas).
![sql](imagenes/reto7.jpg)
## Debe incluir como mínimo el código (product_code), el nombre del producto (product_name), la fecha truncada (transaction_created_date), la descripción del tipo de movimiento (type name) y la suma de cantidad (quantity) .

## 8- Genere la consulta SQL para un reporte de inventario, tomando como base todos los movimientos de inventario (inventory_transactions), considere los tipos de movimiento (transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso. 
   ![sql](imagenes/reto8.jpg)
## Este reporte debe estar agrupado por producto (products) y deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la sumarización de ingresos, salidas y la cantidad disponible en inventario (diferencia de ingresos - salidas).
