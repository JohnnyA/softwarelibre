# Intalacion de laravel from scratch
## link de laravel: https://laravel.com/docs/8.x
## Para iniciar con la instalacion lo primero que tiene que hacer es verificar si ya tienes los siguientes
## requerimientos.
![VirtualBox](imagenes/requeriminetos.jpg)
## seguidamente se procede con la instalacion, lo puede hacer con el sguiente comando.
### composer global require laravel/installer
### Luego tiene que cofigurar laravel de manera global, para eso puede guiarse con la siguente imagen.
![VirtualBox](imagenes/cofiguracionG.jpg)
### al terminar de hacer estos pasos tendras laravel instalado.
### pordras ver tu proyecto con el sguiente comando "php artisan serve"

# Laravel From The Scratch  Routing
### luego de haber creado el proyecto el laravel prodras ver como se carga la pagina, esto se puede en contrar en la siguiente ruta "routes/web.php"
### al entrar en esa ruta podras ver los siguientes datos.
![VirtualBox](imagenes/routes.jpg)
### para ver la funcion que tienen estas rutas crearemos un nuevo archivo que se llamara "test.blade,php"
### en la siguiente ruta resources/views 
![VirtualBox](imagenes/test.jpg)
### detro de test se hara una pequeña prueba, que se mostrar en la siguiente imagen.
![VirtualBox](imagenes/prueba.jpg)

# Laravel From The Scratch Database AccessLección
 ### el primer paso sera buscar el archivo de cofiguracion de la base de datos.
 ### lo podras encontrar en el archivo .env, que encontraras los siguientes datos
 ![VirtualBox](imagenes/bd.jpg)
 ### la cual deberas cofigurar con sus respectivos datos.

 # Laravel From The Scratch Views.
 ## para ver como funcionan las vistas se crearan algunos ejemplos.
 ### se crea "contact.blade.php" contendra lo siguiente
  ![VirtualBox](imagenes/contact.jpg)
 ### y dentro de archivo web.php se agregara lo siguiente para poder llamar a conctac.blade.php
 ![VirtualBox](imagenes/contac.jpg)

# Laravel From The Scratch  Forms
### para empezar se creara un archivo con el nombre de ArticlesController
### se le agregaran los siguientes datos.
 ![VirtualBox](imagenes/arti1.jpg)
  ![VirtualBox](imagenes/arti2.jpg)
  ![VirtualBox](imagenes/arti3.jpg)

### tambien se creara una nueva caperta dentro de views que se llamara "articles" la cual tendra los siguientes datos.
 ![VirtualBox](imagenes/carpeta.jpg)
 ### el cual podras agregar en cada uno sus repectivos metodos.

### En el siguinte link podras encontrar el repositorio del proyecto. https://gitlab.com/JohnnyA/softwarelibre
 





