<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome');
    return 'Hola';
});
*/
Route::get('/', function () {
        $name = request('name');
    return view('test',[
        'name' => $name
    ]);
    
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('articles/{article}','ArticleController@show');
Route::get('articles/{article}','ArticleController@show');
Route::get('/articles/{article}/edit','ArticleController@edit');
Route::get('articles/{article}','ArticleController@update');