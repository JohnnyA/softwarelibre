<?php

namespace App\Http\Controllers;

use App\Article;

class ArticleController extends Controller{

    //Render the list
    public function index(){
    $articles = Article::lastest()->get();
    
    return view('articles.index', ['articles' => $articles]);
    }

    // Show single
    public function show(Article $article){
    $article = Article::findOrFail($id);
    Article::where('id',1)->first();
    return $article;

    return view('articles.show', ['articles' => $article]);
    }

    //Show a view to create
    public function create(){
        return view('articles.create');

    }

    // Persist the new resource
    public function store(){

        request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required'
        ]);
        
       Article::create([
           'title' => request('title'),
           'excerpt' => request('excerpt'),
           'body' => request('body')
       ]);

        return redirect('/articles');

    }
 
    // Show view to edit
    public function edit(Article $article){

        return view('articles.edit', compact('article'));
    }


     // Show view to update
    public function update(Article $article){
       $validateAttributes = request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required'
        ]);

        $article->update($validateAttributes);

      //  $article->title = request('title');
        //$article->excerpt = request('excerpt');
        //$article->body = request('body');
        //$article->save();

        return redirect('/articles/'. $article->id);
    }
    protected function validateArticle(){
        return request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required'
        ]);
    }

     // Delete the resource
    public function destroy(){

    }
}
