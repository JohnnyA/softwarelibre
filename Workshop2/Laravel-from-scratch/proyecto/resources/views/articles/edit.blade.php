@extends('layaout')

@section('content')
<div id="wrapper">
    <div id="page" class="container">
    <h1>Edit Article</h1>
    </div>

<form method="POST" action="/articles/{{$article->id}}">
        @csrf
        @method('PUT')
        <div class="field">
            <label class="label" for="title">Title</label>
            <div class="control">
            <input class="input" type="text" name="title" id="title" vale="{{ $article->title }}">
        </div>
    </div>

        <div class="field is-grouped">
            
            <div class="control">
                <button class="button is-link" type="submit">Edit</button>
        </div>
    </form>
</div>
@endsection