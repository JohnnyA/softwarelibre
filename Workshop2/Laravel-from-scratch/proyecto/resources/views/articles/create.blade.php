@extends('layaout');
@section('head')
@endsection
@section('content');

<div id="wrapper">
    <div id="page" class="container">
    <h1>New Article</h1>
    </div>

    <form method="POST" action="/articles">
        @csrf
        <div class="field">
            <label class="label" for="title">Title</label>
            <div class="control">
                <input class="input" type="text" name="" id="" value="{{ old('title')}}" required>
                @if ($errors->has('title'))
                <p class="help is-danger">{{ $errors->first('title')}}</p>
                @endif
        </div>

        <div class="field is-grouped">
            
            <div class="control">
                <button class="button is-link" type="submit">Save</button>
        </div>
    </form>
</div>
@endsection