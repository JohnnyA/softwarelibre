# Bash ScriptingPágina

### Lo primero que se va hacer el levantar vagrant con el siguiente comando "vagrant up".
### seguidamente se digita "eval $(ssh-agent -s)" para levantar el agente.
### luego el siguiente direccion para agregar la llave "ssh-add .vagrant/machines/default/virtualbox/private_key"
### entramos a vagrantbuster con este comando "ssh -p 2222 vagrant@localhost"
### cuando estamos dentro de vagrantbuster podemos crear un nuevo usuario con el siguiente comando "sudo adduser usuario"
### para poder ingresar con el usuario creado "su usuario" y digitamos los datos que nos pide

### seguidamente se realizara un ejercicio.
1. crear un archivo llamado saludar.sh con el comando "touch saludar.sh"
2. se edita el archivo "vim saludar.sh"
![demo](imagenes/ejemplo.jpg)
3. para darle permisos de ejecucion al archivo se ejecuta este comando "chmod +x saludar.sh"
4. se ejecuta el archivo "./saludar.sh"

### Acontinuacion procedemos a ejecuatar los siguinetes comandos para crear una nueva ruta y mover el archivo saludar.sh 
1. "sudo mkdir -p /opt/"
2. "sudo mkdir -p /opt/scripts/"
3.  "sudo mv saludar.sh /opt/scripts/" se mueve el archivo
4.  "ls -l /opt/scripts/saludar.sh" se verifica que el archivo se encuentre donde los hemos movido.
5.  "sudo chown root:root /opt/scripts/ -R" para cambiar el usuario de vagran a root, esto se hace para que otros usuarios lo puedan utilizar.
6. se ejecuta el siguiente comando para crear un enlace simbolico " sudo ln -s /opt/scripts/saludar.sh /usr/bin/saludar"

# Demo 2, Conectarse a vagran por medio de un archivo llamado "conectar.sh", que contendra los siguientes datos.
![conectar.sh](imagenes/conectar.jpg)
### lo ejecutamos de la siguiente manera "./conectar.sh"

### seguidamente lo que se hara es los archivos northwin que se descargaron, se moveran a vagrant@buster con el siguiente comando "scp northwind*sql vagrant@buster:"

### nos conectamos a la base de datos "sudo mysql"
### se crea una base de datos "create database northwind;"
### se crea un nuevo usuario "create user laravel@'%' identified by 'secret';"
### se agregan los proilegios "grant all privileges on northwind.* to laravel@'%';"
### se refrescan los privilegios "flush privileges;" 

### Acontinuacion se creara un nuevo archivo dentro de la carpeta scripts "touch backup.sh"
![backup.sh](imagenes/backup.jpg)
### final mente se pone a prueba.


